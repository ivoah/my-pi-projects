#!/usr/bin/env python

import cwiid
from time import sleep

def connect():
	print 'Press the 1 + 2 buttons on your wiimote'
	wm = cwiid.Wiimote()
	sleep(1)
	print 'Connected!'
	return wm

wm = connect()

wm.led = 0
st = 0.1
wm.rpt_mode = cwiid.RPT_BTN

while True:
	if wm.state['buttons'] == cwiid.BTN_HOME: break
	wm.led = 1
	sleep(st)
	wm.led = 2
	sleep(st)
	wm.led = 4
	sleep(st)
	wm.led = 8
	sleep(st)
	wm.led = 4
	sleep(st)
	wm.led = 2
	sleep(st)
