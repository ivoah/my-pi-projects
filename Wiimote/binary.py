#!/usr/bin/env python

import cwiid
from time import sleep

def connect():
	print 'Press the 1 + 2 buttons on your wiimote'
	wm = cwiid.Wiimote()
	sleep(1)
	print 'Connected!'
	wm.led = 1
	wm.rumble = True
	sleep(0.1)
	wm.rumble = False
	sleep(0.1)	
	wm.led = 2
	wm.rumble = True
	sleep(0.1)
	wm.rumble = False
	sleep(0.1)	
	wm.led = 4
	wm.rumble = True
	sleep(0.1)	
	wm.rumble = False
	sleep(0.1)	
	wm.led = 8	
	wm.rumble = True
	sleep(0.1)
	wm.rumble = False	
	
	return wm

wm = connect()

wm.led = 0

wm.rpt_mode = cwiid.RPT_BTN

oled = 0

while True:
	btn = wm.state['buttons']
	led = wm.state['led']

	if btn == cwiid.BTN_LEFT or btn == cwiid.BTN_MINUS or btn == cwiid.BTN_2 or btn == cwiid.BTN_DOWN or btn == cwiid.BTN_B: led -= 1	
	if btn == cwiid.BTN_RIGHT or btn == cwiid.BTN_PLUS or btn == cwiid.BTN_1 or btn == cwiid.BTN_UP or btn == cwiid.BTN_A: led += 1
	
	if btn == cwiid.BTN_HOME: break
	
	wm.led = led
	if oled != led:
		print led
		wm.rumble = True
	oled = led
		
	sleep(0.1)
	wm.rumble = False
