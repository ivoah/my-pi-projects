#!/usr/bin/env python
import cwiid

def connect():
	print 'Press the 1 + 2 buttons on your wiimote'
	wm = cwiid.Wiimote()
	sleep(1)
	print 'Connected!'
	return wm

wm = connect()

wm.rpt_mode = cwiid.RPT_BTN

while True:
		if wm.state['buttons']: wm.rumble = True
		else: wm.rumble = False