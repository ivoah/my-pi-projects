#!/usr/bin/env python

from time import sleep
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

'''
   ____
  | 22 |
27|    |23
  |____|
  | 24 |
17|    |25
  |____| .
    4     8
'''

nums = [
	[23, 22, 27, 17, 4, 25],
	[17, 27],
	[22, 23, 24, 17, 4],
	[22, 23, 24, 25, 4],
	[27, 24, 23, 25],
	[22, 27, 24, 25, 4],
	[22, 27, 17, 4, 25, 24],
	[22, 23, 25],
	[22, 27, 24, 25, 4, 17, 23],
	[24, 27, 22, 23, 25, 4],
	[8]
]

for num in nums:
	for pin in num:
		GPIO.setup(pin, GPIO.OUT)

def num(n, on = True):
	for pin in nums[n]:
		GPIO.output(pin, on)

def blink(pin, interval = 0.5):
	GPIO.output(pin, True)
	sleep(interval)
	GPIO.output(False)

def demo():
	for i in range(11):
		num(i, True)
		sleep(0.5)
		num(i, False)

if __name__ == '__main__':
	demo()
