#!/usr/bin/env python
import RPi.GPIO as GPIO from time import sleep GPIO.setmode(GPIO.BCM) GPIO.setup(7, GPIO.OUT) GPIO.setup(10, 
GPIO.OUT) GPIO.setup(27, GPIO.OUT) try:
    while True:
        GPIO.output(7, GPIO.LOW)
        GPIO.output(27, GPIO.HIGH)
        sleep(1)
        GPIO.output(27, GPIO.LOW)
        GPIO.output(10, GPIO.HIGH)
        sleep(0.25)
        GPIO.output(10, GPIO.LOW)
        GPIO.output(7, GPIO.HIGH)
        sleep(1) except KeyboardInterrupt:
    print '\rBye!'
    GPIO.cleanup()
