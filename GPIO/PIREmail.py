import RPi.GPIO as GPIO
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.mime.text import MIMEText
from smtplib import SMTP
from subprocess import call
from time import sleep, strftime
import keyring
from getpass import getpass

GPIO.setmode(GPIO.BCM)

SIG = 17
VCC = 27
GND = 22

GPIO.setup(SIG, GPIO.IN) 
GPIO.setup(VCC, GPIO.OUT)
GPIO.setup(GND, GPIO.OUT)
GPIO.output(VCC, GPIO.HIGH)
GPIO.output(GND, GPIO.LOW)

os = 1

TO = 'my1rock@yahoo.com'
GMAIL_USER = 'ivoahivoah@gmail.com'
GMAIL_PASS = keyring.get_password('PIR', GMAIL_USER)
if GMAIL_PASS is None:
    GMAIL_PASS = getpass()

keyring.set_password('PIR', GMAIL_USER, GMAIL_PASS)

smtpserver = SMTP("smtp.gmail.com",587)
smtpserver.ehlo()
smtpserver.starttls()
smtpserver.ehlo
smtpserver.login(GMAIL_USER, GMAIL_PASS)

def send_email():
    print('Sending Email')
    call(['fswebcam', 'intruder.jpeg'])
    msg = MIMEMultipart()
    msg['Subject'] = 'Intruder!!!'
    msg['From'] = GMAIL_USER
    msg['To'] = TO
    msg.attach(MIMEText('PIR Sensor triggered on {0}, picture of intruder:\n'.format(strftime("%c"))))
    msg.attach(MIMEImage(file('intruder.jpeg').read()))
    smtpserver.sendmail(GMAIL_USER, TO, msg.as_string())
    print('Sent!')

sleep(10)
print('Sentry active!')

try:
    while True:
        s = GPIO.input(SIG)
        if s != os and s == 1:
            send_email()
            sleep(5)
        os = s
except KeyboardInterrupt:
    pass

GPIO.cleanup()
smtpserver.close()
print('\nBye!')